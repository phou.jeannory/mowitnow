package converters;

import models.Lawnmower;
import models.Position;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LawnmowerConverterTest {

    private LawnmowerConverter converter;

    @Before
    public void setUp() throws Exception {
        converter = new LawnmowerConverter();
    }


    @Test
    public void toLawnmowerShouldReturnLawnmover1() {
        //given
        final List<String> inputs = Arrays.asList("1 2 N", "GAGAGAGAA");
        final Lawnmower expect = new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A'));

        //when
        final Lawnmower result = converter.toLawnmower(inputs);

        //then
        Assert.assertEquals(expect.toString(), result.toString());
    }

    @Test
    public void toLawnmowerShouldReturnLawnmover2() {
        //given
        List<String> inputs = Arrays.asList("3 3 E", "AADAADADDA");
        final Lawnmower expect = new Lawnmower(new Position(3, 3, 'E'), Arrays.asList('A', 'A', 'D', 'A', 'A', 'D', 'A', 'D', 'D', 'A'));

        //when
        final Lawnmower result = converter.toLawnmower(inputs);

        //then
        Assert.assertEquals(expect.toString(), result.toString());
    }

    @Test
    public void toLawnmowerShouldReturnLawnmover1WhenFirstLineThrowNeWConverterException() {
        //given
        final List<String> inputs = Arrays.asList("5 5", "1 2 N", "GAGAGAGAA");
        final Lawnmower expect = new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A'));

        //when
        final Lawnmower result = converter.toLawnmower(inputs);

        //then
        Assert.assertEquals(expect.toString(), result.toString());
    }

    @Test
    public void toLawnmowerShouldReturnLawnmover1WhenTwoFirstLineThrowNeWConverterException() {
        //given
        final List<String> inputs = Arrays.asList("0,0", "5 5", "1 2 N", "GAGAGAGAA");
        final Lawnmower expect = new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A'));

        //when
        final Lawnmower result = converter.toLawnmower(inputs);

        //then
        Assert.assertEquals(expect.toString(), result.toString());
    }

    @Test
    public void toLawnmowersShouldReturnLawnmovers() {
        //given
        final List<String> inputs = Arrays.asList("1 2 N", "GAGAGAGAA", "3 3 E", "AADAADADDA");
        final List<Lawnmower> expects = new ArrayList<>();
        final Lawnmower expect1 = new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A'));
        final Lawnmower expect2 = new Lawnmower(new Position(3, 3, 'E'), Arrays.asList('A', 'A', 'D', 'A', 'A', 'D', 'A', 'D', 'D', 'A'));
        expects.add(expect1);
        expects.add(expect2);

        //when
        final List<Lawnmower> results = converter.toLawnmowers(inputs);

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

}