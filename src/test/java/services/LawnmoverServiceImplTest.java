package services;

import models.Lawnmower;
import models.Location;
import models.Position;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LawnmoverServiceImplTest {

    private LawnmoverService lawnmoverService;

    @Before
    public void setUp() throws Exception {
        lawnmoverService = new LawnmoverServiceImpl();
    }

    @Test
    public void moveIntoLawnShouldReturnExpects() {
        //given
        final Location lawnMaxValues = new Location(5, 5);
        final List<Lawnmower> lawnmowers = Arrays.asList(
                new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A')),
                new Lawnmower(new Position(3, 3, 'E'), Arrays.asList('A', 'A', 'D', 'A', 'A', 'D', 'A', 'D', 'D', 'A')));

        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(1, 3, 'N'));
        expects.add(new Position(5, 1, 'E'));

        //when
        final List<Position> results = lawnmoverService.moveIntoLawn(lawnMaxValues, lawnmowers);

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void moveIntoLawnShouldNotReturnNegativeValuedForXAndY() {
        //given
        final Location lawnMaxValues = new Location(5, 5);
        final List<Lawnmower> lawnmowers = Arrays.asList(
                new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'A', 'G', 'A', 'A', 'A')),
                new Lawnmower(new Position(3, 3, 'E'), Arrays.asList('D', 'A', 'A', 'A', 'A', 'D', 'A', 'A', 'A', 'A')));

        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(0, 0, 'S'));
        expects.add(new Position(0, 0, 'W'));

        //when
        final List<Position> results = lawnmoverService.moveIntoLawn(lawnMaxValues, lawnmowers);

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void moveIntoLawnShouldNotGoOutSideTheLawn() {
        //given
        final Location lawnMaxValues = new Location(3, 3);
        final List<Lawnmower> lawnmowers = Arrays.asList(
                new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('A', 'A', 'A', 'A', 'D', 'A', 'A', 'A')),
                new Lawnmower(new Position(3, 3, 'E'), Arrays.asList('A', 'A', 'G', 'A', 'A', 'A', 'A', 'A', 'A')));

        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(3, 3, 'E'));
        expects.add(new Position(3, 3, 'N'));

        //when
        final List<Position> results = lawnmoverService.moveIntoLawn(lawnMaxValues, lawnmowers);

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void moveIntoLawnShouldReturnExpectsWhenSomeInstructionNoExist() {
        //given
        final Location lawnMaxValues = new Location(5, 5);
        final List<Lawnmower> lawnmowers = Arrays.asList(
                new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A', 'X')),
                new Lawnmower(new Position(3, 3, 'E'), Arrays.asList('A', 'A', 'D', 'A', 'A', 'D', 'A', 'D', 'D', 'A', 'T')));

        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(1, 3, 'N'));
        expects.add(new Position(5, 1, 'E'));

        //when
        final List<Position> results = lawnmoverService.moveIntoLawn(lawnMaxValues, lawnmowers);

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void moveIntoLawnShouldReturnInitialPositionWhenAllInstructionNoExist() {
        //given
        final Location lawnMaxValues = new Location(5, 5);
        final List<Lawnmower> lawnmowers = Arrays.asList(
                new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('M', 'V', 'Z', 'X', 'X', 'M', 'V', 'Z', 'X', 'X')),
                new Lawnmower(new Position(3, 3, 'E'), Arrays.asList('M', 'V', 'Z', 'X', 'X', 'M', 'V', 'Z', 'X', 'X')));
        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(1, 2, 'N'));
        expects.add(new Position(3, 3, 'E'));


        //when
        final List<Position> results = lawnmoverService.moveIntoLawn(lawnMaxValues, lawnmowers);

        //then
        Assert.assertEquals(expects.toString(), results.toString());

    }

    @Test
    public void moveIntoLawnShouldReturnOnePositionAndThrowPositionExceptionWhenOrientationNoExist() {
        //given
        final Location lawnMaxValues = new Location(5, 5);
        final List<Lawnmower> lawnmowers = Arrays.asList(
                new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A')),
                new Lawnmower(new Position(3, 3, 'X'), Arrays.asList('A', 'A', 'D', 'A', 'A', 'D', 'A', 'D', 'D', 'A')));

        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(1, 3, 'N'));

        //when
        final List<Position> results = lawnmoverService.moveIntoLawn(lawnMaxValues, lawnmowers);

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void moveIntoLawnShouldReturnCollectionsEmptyListAndThrowPositionExceptionWhenAllOrientationNoExist() {
        //given
        final Location lawnMaxValues = new Location(5, 5);
        final List<Lawnmower> lawnmowers = Arrays.asList(
                new Lawnmower(new Position(1, 2, 'X'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A', 'X')),
                new Lawnmower(new Position(3, 3, 'X'), Arrays.asList('A', 'A', 'D', 'A', 'A', 'D', 'A', 'D', 'D', 'A', 'T')));

        //when
        final List<Position> results = lawnmoverService.moveIntoLawn(lawnMaxValues, lawnmowers);

        //then
        Assert.assertEquals(Collections.emptyList(), results);
    }

    @Test
    public void moveIntoLawnShouldReturnExpectsWhenContainsOtherChar() {
        //given
        final Location lawnMaxValues = new Location(5, 5);
        final List<Lawnmower> lawnmowers = Arrays.asList(
                new Lawnmower(new Position(1, 2, 'N'), Arrays.asList('G', 'A', 'G', 'A', 'G', 'A', 'G', 'A', 'A', '$', ' ', '8', '*', '$')),
                new Lawnmower(new Position(3, 3, 'E'), Arrays.asList('A', 'A', 'D', 'A', 'A', 'D', 'A', 'D', 'D', 'A', '$', ' ', '8', '*', '$')));

        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(1, 3, 'N'));
        expects.add(new Position(5, 1, 'E'));

        //when
        final List<Position> results = lawnmoverService.moveIntoLawn(lawnMaxValues, lawnmowers);

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void toLawnmowerAndmoveIntoLawnShouldReturnExpects() {
        //given
        final List<String> inputs = Arrays.asList("5 5", "1 2 N", "GAGAGAGAA", "3 3 E", "AADAADADDA");

        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(1, 3, 'N'));
        expects.add(new Position(5, 1, 'E'));

        //when
        final List<Position> results = lawnmoverService.toLawnmowerAndmoveIntoLawn(inputs);

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void toLawnmowerAndmoveIntoLawnShouldReturnExpectsFromFile1() {
        //given
        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(1, 3, 'N'));
        expects.add(new Position(5, 1, 'E'));

        //when
        final List<Position> results = lawnmoverService.toLawnmowerAndmoveIntoLawn("inputs/file-1.txt");

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void toLawnmowerAndmoveIntoLawnShouldReturnExpectsFromFile2WhenXIsALetter() {
        //given
        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(5, 1, 'E'));

        //when
        final List<Position> results = lawnmoverService.toLawnmowerAndmoveIntoLawn("inputs/file-2.txt");

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void toLawnmowerAndmoveIntoLawnShouldReturnExpectsFromFile3WhenYIsALetter() {
        //given
        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(5, 1, 'E'));

        //when
        final List<Position> results = lawnmoverService.toLawnmowerAndmoveIntoLawn("inputs/file-3.txt");

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void toLawnmowerAndmoveIntoLawnShouldReturnExpectsFromFile4WhenOrientationIs9() {
        //given
        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(5, 1, 'E'));

        //when
        final List<Position> results = lawnmoverService.toLawnmowerAndmoveIntoLawn("inputs/file-4.txt");

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void toLawnmowerAndmoveIntoLawnShouldNotReturnNegativeValuedForXAndY() {
        //given
        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(0, 0, 'S'));
        expects.add(new Position(0, 0, 'W'));

        //when
        final List<Position> results = lawnmoverService.toLawnmowerAndmoveIntoLawn("inputs/file-5.txt");

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

    @Test
    public void toLawnmowerAndmoveIntoLawnShouldNotGoOutSideTheLawn() {
        //given
        final List<Position> expects = new ArrayList<>();
        expects.add(new Position(0, 0, 'S'));
        expects.add(new Position(0, 0, 'W'));

        //when
        final List<Position> results = lawnmoverService.toLawnmowerAndmoveIntoLawn("inputs/file-6.txt");

        //then
        Assert.assertEquals(expects.toString(), results.toString());
    }

}