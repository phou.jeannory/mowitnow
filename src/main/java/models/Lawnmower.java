package models;


import java.util.List;

public class Lawnmower {

    private Position position;
    private List<Character> instructions;

    public Lawnmower() {
    }

    public Lawnmower(Position position, List<Character> instructions) {
        this.position = position;
        this.instructions = instructions;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public List<Character> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<Character> instructions) {
        this.instructions = instructions;
    }

    @Override
    public String toString() {
        return "Lawnmower{" +
                "position=" + position.toString() +
                ", instructions=" + instructions +
                '}';
    }
}
