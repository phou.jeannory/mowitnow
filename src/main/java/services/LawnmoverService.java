package services;

import models.Lawnmower;
import models.Location;
import models.Position;

import java.util.List;

public interface LawnmoverService {

    List<Position> moveIntoLawn(Location lawnMaxValues, List<Lawnmower> lawnmowers);

    List<Position> toLawnmowerAndmoveIntoLawn(List<String> inputs);

    List<Position> toLawnmowerAndmoveIntoLawn(String path);
}
