package services;

import converters.LawnmowerConverter;
import exceptions.PositionException;
import models.Lawnmower;
import models.Location;
import models.Position;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LawnmoverServiceImpl implements LawnmoverService {

    private LawnmowerConverter converter;

    /**
     * @param , Max values for lawn and List of Lawnmower
     *          which move following instructions
     * @return final positions in the lawn
     */
    @Override
    public List<Position> moveIntoLawn(Location lawnMaxValues, List<Lawnmower> lawnmowers) {
        int i = 0;
        final List<Position> positions = new ArrayList<>();
        while (i < lawnmowers.size()) {
            int j = 0;
            try {
                //initial position
                Position position = validateOrientation(lawnmowers.get(i).getPosition());
                while (j < lawnmowers.get(i).getInstructions().size()) {
                    if (isValidMove(lawnMaxValues, position, lawnmowers.get(i).getInstructions().get(j))) {
                        position = toMove(position, lawnmowers.get(i).getInstructions().get(j));
                    }
                    j += 1;
                }
                //final position
                positions.add(position);
            } catch (PositionException ex) {
                System.out.println("Custom exception : " + ex.getMessage());
            }
            i += 1;
        }
        return positions;
    }

    public boolean isValidMove(Location lawnMaxValues, Position position, char instruction) {
        boolean isValidMove = true;
        if (instruction == 'A') {
            final Position nextPosition = toMove(position, instruction);
            if (
                    nextPosition.getX() > lawnMaxValues.getX() ||
                            nextPosition.getY() > lawnMaxValues.getY() ||
                            nextPosition.getX() < 0 ||
                            nextPosition.getY() < 0
            ) {
                isValidMove = false;
            }
        }
        return isValidMove;
    }

    @Override
    public List<Position> toLawnmowerAndmoveIntoLawn(List<String> inputs) {
        converter = new LawnmowerConverter();
        final String[] lawnMaxValues = inputs.get(0).split(" ");
        return moveIntoLawn(
                new Location(Integer.parseInt(lawnMaxValues[0]), Integer.parseInt(lawnMaxValues[1])),
                converter.toLawnmowers(inputs)
        );
    }

    @Override
    public List<Position> toLawnmowerAndmoveIntoLawn(String path) {
        List<String> inputs = null;
        try (Stream<String> lines = Files.lines(Paths.get(path))) {
            inputs = lines.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return toLawnmowerAndmoveIntoLawn(inputs);
    }


    private Position toMove(Position position, char instruction) {
        final Position nextPosition = new Position(position.getX(), position.getY(), position.getOrientation());

        //go forward
        if (instruction == 'A') {
            switch (nextPosition.getOrientation()) {
                case 'N':
                    nextPosition.setY(position.getY() + 1);
                    break;
                case 'E':
                    nextPosition.setX(position.getX() + 1);
                    break;
                case 'S':
                    nextPosition.setY(position.getY() - 1);
                    break;
                case 'W':
                    nextPosition.setX(position.getX() - 1);
                    break;
            }
        }

        //go to the right
        else if (instruction == 'D') {
            switch (nextPosition.getOrientation()) {
                case 'N':
                    nextPosition.setOrientation('E');
                    break;
                case 'E':
                    nextPosition.setOrientation('S');
                    break;
                case 'S':
                    nextPosition.setOrientation('W');
                    break;
                case 'W':
                    nextPosition.setOrientation('N');
                    break;
            }
        }

        //go to the left
        else if (instruction == 'G') {
            switch (nextPosition.getOrientation()) {
                case 'N':
                    nextPosition.setOrientation('W');
                    break;
                case 'W':
                    nextPosition.setOrientation('S');
                    break;
                case 'S':
                    nextPosition.setOrientation('E');
                    break;
                case 'E':
                    nextPosition.setOrientation('N');
                    break;
            }
        }
        return nextPosition;
    }

    private Position validateOrientation(Position position) {
        if (
                position.getOrientation() == 'N' ||
                        position.getOrientation() == 'E' ||
                        position.getOrientation() == 'S' ||
                        position.getOrientation() == 'W'
        ) {
            return position;
        } else {
            throw new PositionException("Orientation is not valid");
        }
    }
}
