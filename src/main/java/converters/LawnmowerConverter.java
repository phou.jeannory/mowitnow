package converters;

import exceptions.ConverterException;
import models.Lawnmower;
import models.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LawnmowerConverter {

    /**
     * convert lines into List of Lawnmower
     */
    public List<Lawnmower> toLawnmowers(List<String> inputs) {
        final List<Lawnmower> lawnmowers = new ArrayList<>();
        int i = 0;
        while (i < inputs.size()) {

            try {
                final List<String> validInputs = new ArrayList<>();
                validInputs.add(validatePosition(inputs.get(i)));
                i += 1;
                validInputs.add(inputs.get(i));
                i += 1;
                lawnmowers.add(toLawnmower(validInputs));
            } catch (ConverterException ex) {
                System.out.println("Converter Exception : " + ex.getMessage());
                i += 1;
            }
        }
        return lawnmowers;
    }

    /**
     * @param , first line contains position, second line contains instructions
     * @return Lawnmower
     */
    public Lawnmower toLawnmower(List<String> inputs) {
        try {
            final List<String> validInputs = validateInputs(inputs);
            final String line = validInputs.get(0);
            final String[] positions = line.split(" ");
            final Position position = new Position(Integer.parseInt(positions[0]), Integer.parseInt(positions[1]), positions[2].charAt(0));
            final List<Character> instructions = validInputs.get(1).chars().mapToObj(c -> (char) c).collect(Collectors.toList());
            return new Lawnmower(position, instructions);
        } catch (ConverterException ex) {
            System.out.println("Converter Exception : " + ex.getMessage());
            return null;
        }
    }

    private List<String> validateInputs(List<String> inputs) {
        int i = 0;
        final List<String> validInputs = new ArrayList<>();
        while (i < inputs.size()) {
            try {
                validInputs.add(validatePosition(inputs.get(i)));
                i += 1;
                validInputs.add(inputs.get(i));
                i += 1;
            } catch (ConverterException ex) {
                System.out.println("Converter Exception : " + ex.getMessage());
                i += 1;
            }
        }
        return validInputs;
    }

    private String validatePosition(String input) {
        if (
                input.length() == 5 &&
                        input.charAt(1) == ' ' &&
                        input.charAt(3) == ' '
        ) {
            try {
                Integer.parseInt(String.valueOf(input.charAt(0)));
                Integer.parseInt(String.valueOf(input.charAt(2)));
            } catch (NumberFormatException ex) {
                throw new ConverterException("X and Y must be an integer");
            }

            if (input.charAt(4) == 'N' || input.charAt(4) == 'E' || input.charAt(4) == 'S' || input.charAt(4) == 'W') {
                System.out.println("position is valid");
            } else {
                throw new ConverterException("orientation accept only NESW");
            }
            return input;
        } else {
            throw new ConverterException("not a position");
        }

    }

}
